const express=require('express')
const cors=require('cors')
const mysql=require('mysql')

const connection=mysql.createConnection({
    host:'172.31.240.1',
    user:'root',
    password:'root',
    waitForConections:true,
    database:'student-tb'
})

const app = express();
app.use(cors('*'))
app.use(express.json());
connection.connect();

//Get details
app.get('/', (req, res)=>
{
    const query =`Select * from student-tb`;
    connection.query(query, (error, result)=>
    {
        if(error!=null)
        {
            res.send(error)
        }
        else
        {
            res.send(result)
        }
    })  
})


//Add Details
app.post('/', (req, res)=>
{
    const query =`insert into student_tb values('${req.body.std_id}', '${req.bodys_name}','${req.bodypassword}','${req.body.course}','${req.body.passing_year}','${req.body.prn_no}','${req.body.dob}') `;
    connection.query(query, (error, result)=>
    {
        if(error!=null)
        {
            res.send(error)
        }
        else
        {
            res.send(result)
        }
    })  
})


//Update details
app.put('/:std_id', (req, res)=>
{
    const query =`update student_tb set course='${req.body.course}', prn_no='${req.body.prn_no}' where std_id='${req.params.std_id}'`;
    connection.query(query, (error, result)=>
    {
        if(error!=null)
        {
            res.send(error)
        }
        else
        {
            res.send(result)
        }
    })  
})

//student_tb(std_id int primary key, s_name varchar(40), password varchar(20), 
//course varchar(20), passing_year int, prn_no int, dob date)
//Delete
app.delete('/:std_id', (req, res)=>
{
    const query =`delete from student_tb where std_id='${req.params.std_id}'`;
    connection.query(query, (error, result)=>
    {
        if(error!=null)
        {
            res.send(error)
        }
        else
        {
            res.send(result)
        }
    })  
})